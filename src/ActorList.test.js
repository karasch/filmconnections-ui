import renderer from 'react-test-renderer';
import ActorList from "./ActorList";

const actors = {
    "nm0000323": {
      "id": "nm0000323",
      "name": "Michael Caine",
      "picture": "https://imdb-api.com/images/original/MV5BMjAwNzIwNTQ4Ml5BMl5BanBnXkFtZTYwMzE1MTUz._V1_Ratio0.7273_AL_.jpg"
    },
    "nm0614165": {
      "id": "nm0614165",
      "name": "Cillian Murphy",
      "picture": "https://imdb-api.com/images/original/MV5BMTUzMjg1NzIyOV5BMl5BanBnXkFtZTYwMzg2Mjgy._V1_Ratio0.7273_AL_.jpg"
    },
    "nm0913822": {
      "id": "nm0913822",
      "name": "Ken Watanabe",
      "picture": "https://imdb-api.com/images/original/MV5BMTQzMTUzNjc4Nl5BMl5BanBnXkFtZTcwMTUyODU2Mw@@._V1_Ratio0.7273_AL_.jpg"
    },
    "nm0687091": {
      "id": "nm0687091",
      "name": "Andrew Pleavin",
      "picture": "https://imdb-api.com/images/original/MV5BMTIwMTIzNTk4OV5BMl5BanBnXkFtZTcwOTc3NjIzMQ@@._V1_Ratio0.9545_AL_.jpg"
    },
    "nm7419291": {
      "id": "nm7419291",
      "name": "Arnold Montey",
      "picture": "https://imdb-api.com/images/original/nopicture.jpg"
    }
  };

test('Render a list of actors', () => {
    const tree = renderer.create(<ActorList actorList={actors} />).toJSON();
    expect(tree).toMatchSnapshot();
});
import React, { useState } from 'react';
import {Button} from 'react-bootstrap';
import './App.css';
import SearchBox from './SearchBox';
import ActorList from './ActorList';

const App = () => {

  const [firstLink, setFirstLink] = useState("");
  const [secondLink, setSecondLink] = useState("");
  const [commonActors, setCommonActors] = useState({});
  const [comparisonStatus, setComparisonStatus] = useState("none");

  // put the first selected movie in the state
  function selectFirst(movieId){
    console.log("First: " + movieId);
    setFirstLink(movieId);
  }

  // put the second selected movie in the state
  function selectSecond(movieId){
    console.log("Second: " + movieId);
    setSecondLink(movieId);
  }

  function doCompare(){

    // Set up the base of the URLs we will need to retrieve.
    const baseURL = process.env.REACT_APP_API_BASE_URL;
    const titleSub = process.env.REACT_APP_API_TITLE_SUBURL;
    const apiKey = process.env.REACT_APP_API_KEY;
    const titleLink = baseURL + titleSub + apiKey;

    let firstMovieActors = {};
    let newCommonActors = {};

    // For both movies, call the service and get the response as JSON
    // For the first movie, get the actors array from the JSON and put it into an associative array.
    fetch(titleLink + firstLink)
      .then((res) => res.json())
      .then((res) => res.fullCast.actors.map( actor => firstMovieActors[actor.id] = actor.name));

    // For the second movie, check the array from the first movie, and put any actors in common into the common actors object
    // then finally put the common actors object in the state.
    fetch(titleLink + secondLink)
      .then((res) => res.json())
      .then((res) => res.fullCast.actors.map( actor => { if(firstMovieActors.hasOwnProperty(actor.id)) { 
                                                              var common = {}; 
                                                              common.id=actor.id; 
                                                              common.name=actor.name;
                                                              common.picture=actor.image;
                                                              newCommonActors[actor.id] = common; 
                                                            } 
                                                            return true; }))
      .then( () => setCommonActors(newCommonActors) );
      //NOTE: If you don't have the setState call in a .then(), it WILL fire out of order.
  }

  return(
    <div className="App">
      <div className="header">
        <h1>Film Connections</h1>
        <p>
          Compare the casts of movies to see who they have in common.
        </p>
        <p>
          Search for a movie on each side, select your movies from the search results, then click the Compare button.
        </p>
      </div>
      <div className="searchselect">
        <SearchBox onSelect={selectFirst} selectedLink={firstLink} />
        <SearchBox onSelect={selectSecond} selectedLink={secondLink} />
      </div>
      <div><Button disabled={(firstLink === "" || secondLink === "") } onClick={() => {setComparisonStatus("inprogress"); doCompare()}}>Compare</Button></div>
      {comparisonStatus === "none" && <div>Click on Compare</div>}
      {comparisonStatus !== "none" && <ActorList actorList={commonActors} />}
    </div>
  );

}

export default App;

import React, { useState } from 'react';
import {Button, Form, FormControl} from 'react-bootstrap';
import SearchResult from './SearchResult';

const SearchBox = ({onSelect, selectedLink}) => {

    const [searchTerm, setSearchTerm] = useState("");
    const [searchType, setSearchType] = useState("any");
    const [searchResults, setSearchResults] = useState([]);
    const [searchError, setSearchError] = useState("");

    const baseURL = process.env.REACT_APP_API_BASE_URL;
    const searchSub = process.env.REACT_APP_API_SEARCH_SUBURL;
    const apiKey = process.env.REACT_APP_API_KEY;

    // Construct the appropriate search URL and put the results in state.
    function doSearch(){

        let searchService = "Search/"

        switch (searchType) {
        case "film":
            searchService = "SearchMovie/";
            break;
        case "series":
            searchService = "SearchSeries/";
            break;
        default:
            break;
        }


        console.log("baseURL is: " + baseURL);
        console.log("searchSub is: " + searchSub);
        console.log("apiKey is: " + apiKey);

        const searchURL = baseURL + searchSub + searchService + apiKey + searchTerm;
        

        console.log("Search URL is : " + searchURL);

        if(searchTerm !== "")
        fetch(searchURL)
            .then( res => res.json())
            .then( (result) => {setSearchResults(result.results);  },
                    (error) => {setSearchError(JSON.stringify(error)); console.log(error);}
                );
    }

    // bind the search text field to the state
    function handleSearchChange(event){
        if(event != null){
            const updatedSearchTerm = event.target.value.trim();
            if(updatedSearchTerm.length > 3){
                setSearchTerm(updatedSearchTerm);
            }
        }
    }

    // bind the search drop down to the state
    function handleSearchTypeChange(event){
        if(event != null){
            const updatedSearchType = event.target.value;
            setSearchType(updatedSearchType);
        }
    }

    // prevent form submission
    function handleSubmit(event){
        event.preventDefault();
    }

    // Return a div with the form and the search results, if any
    return (
    <div className="halfwidth" >
        <Form onSubmit={handleSubmit}>
            <FormControl type="text" onChange={handleSearchChange} defaultValue={searchTerm} data-testid="SearchText"></FormControl>
            <FormControl as="select" value={searchType} onChange={handleSearchTypeChange}>
                <option value="any">Any Title</option>
                <option value="film">Film Title</option>
                <option value="series">Series Title</option>
            </FormControl>
            <Button onClick={ () => doSearch() }>Search</Button>
            {searchResults.map( (element, index) => <SearchResult key={element.id} title={element.title} link={element.id} description={element.description} onSelect={onSelect} img={element.image} selected={element.id === selectedLink} />)}
        </Form>
    </div>
    );

}

export default SearchBox;
import React from 'react';
import renderer from 'react-test-renderer';
import { render, fireEvent, cleanup } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {act} from 'react-dom/test-utils';
import SearchBox from './SearchBox';

afterEach(cleanup);

test("Render with no search results", () => {
    const tree = renderer.create(<SearchBox />).toJSON();
    expect(tree).toMatchSnapshot();
});

test("Search results displayed", async () => {

    const searchResults = {
        "results": [
            {
                "id": "tt1375666",
                "resultType": "Title",
                "image": "https://imdb-api.com/images/original/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_Ratio0.6800_AL_.jpg",
                "title": "Inception The Movie",
                "description": "(2010)"
            },
            {
                "id": "tt5295894",
                "resultType": "Title",
                "image": "https://imdb-api.com/images/original/MV5BMjE0NGIwM2EtZjQxZi00ZTE5LWExN2MtNDBlMjY1ZmZkYjU3XkEyXkFqcGdeQXVyNjMwNzk3Mjk@._V1_Ratio0.6800_AL_.jpg",
                "title": "Inception: The Cobol Job",
                "description": "(2010 Video)"
            },
            {
                "id": "tt5295990",
                "resultType": "Title",
                "image": "https://imdb-api.com/images/original/MV5BZGFjOTRiYjgtYjEzMS00ZjQ2LTkzY2YtOGQ0NDI2NTVjOGFmXkEyXkFqcGdeQXVyNDQ5MDYzMTk@._V1_Ratio0.6800_AL_.jpg",
                "title": "Inception: Jump Right Into the Action",
                "description": "(2010 Video)"
            }
        ],
    };

    // Set up a mock fetch
    jest.spyOn(global, "fetch").mockImplementation( (url) => {
        return Promise.resolve({
            json: () => Promise.resolve(searchResults)
        });
    });
    

    // Render the component
    let container = render(<SearchBox />);

    // Find the search text input and input some text.
    const textInput = container.getByTestId("SearchText");
    userEvent.type(textInput, "search term 1");

    // Click the search button
    const searchButton = container.getByText("Search");
    fireEvent.click(searchButton);

    await act(async () => {
        container = render(<SearchBox />);
    });

    // Check that the titles show up
    expect(container.getByText(/Inception The Movie/)).toBeInTheDocument();
    expect(container.getByText(/Inception: The Cobol Job/)).toBeInTheDocument();
    expect(container.getByText(/Inception: Jump Right Into the Action/)).toBeInTheDocument();

    // Tear down the mock fetch
    global.fetch.mockRestore();
});

test("Search result clicked", async () => {
    // Set up a mock selected function
    let selectedLink;
    const mockOnSelect = jest.fn((link) => {
        selectedLink = link;
    });

    const searchResults = {
        "results": [
            {
                "id": "tt1375666",
                "resultType": "Title",
                "image": "https://imdb-api.com/images/original/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_Ratio0.6800_AL_.jpg",
                "title": "Inception The Movie",
                "description": "(2010)"
            },
            {
                "id": "tt5295894",
                "resultType": "Title",
                "image": "https://imdb-api.com/images/original/MV5BMjE0NGIwM2EtZjQxZi00ZTE5LWExN2MtNDBlMjY1ZmZkYjU3XkEyXkFqcGdeQXVyNjMwNzk3Mjk@._V1_Ratio0.6800_AL_.jpg",
                "title": "Inception: The Cobol Job",
                "description": "(2010 Video)"
            },
            {
                "id": "tt5295990",
                "resultType": "Title",
                "image": "https://imdb-api.com/images/original/MV5BZGFjOTRiYjgtYjEzMS00ZjQ2LTkzY2YtOGQ0NDI2NTVjOGFmXkEyXkFqcGdeQXVyNDQ5MDYzMTk@._V1_Ratio0.6800_AL_.jpg",
                "title": "Inception: Jump Right Into the Action",
                "description": "(2010 Video)"
            }
        ],
    };

    // Set up a mock fetch
    jest.spyOn(global, "fetch").mockImplementation( (url) => {
        return Promise.resolve({
            json: () => Promise.resolve(searchResults)
        });
    });
    

    // Render the component
    let container = render(<SearchBox onSelect={mockOnSelect} selectedLink={selectedLink} />);

    // Find the search text input and input some text.
    const textInput = container.getByTestId("SearchText");
    userEvent.type(textInput, "search term 2");

    // Click the search button
    const searchButton = container.getByText("Search");
    fireEvent.click(searchButton);

    // Re-render the component
    await act(async () => {
        container = render(<SearchBox onSelect={mockOnSelect} selectedLink={selectedLink} />);
    });

    // Check vs. snapshot
    expect(container).toMatchSnapshot();

    // Get the first result
    const result = container.getByText(/Inception The Movie/);

    // Click on the result
    fireEvent.click(result);

    // Check that onSelect was called
    expect(mockOnSelect).toHaveBeenCalled();

    expect(selectedLink).toBe("tt1375666")

    // Re-re-render the component
    container = render(<SearchBox onSelect={mockOnSelect} selectedLink={selectedLink} />);

    // Check vs. snapshot again
    expect(container).toMatchSnapshot();

});
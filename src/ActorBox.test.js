import React from 'react';
import renderer from 'react-test-renderer';
import ActorBox from './ActorBox';

test('Renders correctly', () => {
    const tree = renderer.create(<ActorBox name="nameymcnameface" id="justakey" picture="linkToPicture" />).toJSON();
    expect(tree).toMatchSnapshot();
});
const ActorBox = ({name, id, picture}) => {

    return(
        <div className="selectbox">
            <div className="thumbcontainer">
                <img alt={name} className="thumbnail" src={picture} />
            </div>
            <div>
                <a href={"//www.imdb.com/name/" + id} target="_blank" rel="noreferrer">{name}</a>
            </div>
        </div>
    );
};

export default ActorBox;
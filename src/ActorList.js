import React from "react";
import ActorBox from "./ActorBox";

const ActorList = ({actorList}) => {

    return(
        <div style={{display: 'inline-block'}} >
            {Object.keys(actorList).length === 0 && <h2>Nothing in common</h2>}
            {Object.keys(actorList).map( key => <ActorBox key={key} name={actorList[key].name} id={key} picture={actorList[key].picture} />)}
        </div>
    );

};

export default ActorList;
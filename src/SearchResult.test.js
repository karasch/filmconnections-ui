import React from 'react';
import renderer from 'react-test-renderer';
import { render, fireEvent, cleanup } from '@testing-library/react';
import SearchResult from './SearchResult';

afterEach(cleanup);

test('Renders correctly', () =>{
    const mockOnSelect = jest.fn();
    const tree = renderer.create(<SearchResult title="titletitle" link="linkybit" description="describeme" img="imagelink" selected="false" onSelect={mockOnSelect} />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});

test('Verify selection', () => {
    const mockOnSelect = jest.fn();
    const container = render(<SearchResult title="title-ybit" link="linkybits" description="test selection" img="image link" selected="false" onSelect={mockOnSelect} />);

    //get one of the sub elements, because using the container doesn't causes an error.
    // Also using container.baseElement doesn't trigger the onClick for some reason.
    const image = container.getByAltText("movie cover");

    fireEvent.click(image);
    
    expect(container).toMatchSnapshot();
    expect(mockOnSelect).toHaveBeenCalled();
});


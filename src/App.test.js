import renderer from 'react-test-renderer';
import App from './App';

test('Render the whole application', () => {
  const tree = renderer.create(<App />).toJSON();
  expect(tree).toMatchSnapshot();
});

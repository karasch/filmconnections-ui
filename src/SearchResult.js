
const SearchResult = ({title, link, description, onSelect, img, selected}) => {

    // A div displaying a movie title, short description (usually the year), and film poster.
    // The background color will change if it has been selected.
    return(
        <div className={`selectbox ${selected? "picked" : ""}`} onClick={() => onSelect(link)} >
            <div className="thumbcontainer">
                <img alt="movie cover" className="thumbnail" src={img} height="100" />
            </div>
            <div>
                {title} {description}
            </div>
        </div>
    );
}

export default SearchResult;